
import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome' //иконки для TabBar
/**
 * ## TabIcon
 * Displays the icon for the tab w/ color dependent upon selection
 */
class TabIcon extends React.Component {
  render () {
    var color = this.props.selected ? '#FF3366' : '#FFB3B3';
    var iconname = this.props.selected ? this.props.iconName : 'cloud'
    return (
      <View style={{flex: 1, flexDirection: 'column', alignItems: 'center', alignSelf: 'center', height:70, paddingTop:13}}>
        <Icon style={{color: color,margin:0, padding:0}} name={iconname} size={35} />
        <Text style={{color: color, margin:0, padding:0, marginTop:-5}}>{this.props.title}</Text>
      </View>
     )
  }
}
module.exports = TabIcon