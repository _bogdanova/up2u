'use strict';

import device from './deviceReducer';
import global from './globalReducer';

import { combineReducers } from 'redux';

const rootReducer = combineReducers({
  device,
  global
});

export default rootReducer;
