
'use strict';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import globalActions from '../actions/global/globalActions';
import {Map} from 'immutable';

import React, {Component} from 'react';
import
{
  StyleSheet,
  View,
  Text
}
from 'react-native';


function mapStateToProps(state) {
    return {
        global: {
            currentUser: state.global.currentUser,
            currentState: state.global.currentState,
            showState: state.global.showState,
        },
    };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({globalActions}, dispatch),
  };
}


class Profile extends Component {

  componentWillReceiveProps(props) {

  }

  componentDidMount() {
  }


  render() {

    let self = this;

    return (
      <View style={styles.container}>
        <View style={styles.inputs}>
          <Text>Profile page</Text>
        </View>

      </View>
    );
  }
}
//export default connect(mapStateToProps, mapDispatchToProps)(Profile);
module.exports = Profile;


/**
 * ## Styles
 */
var styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1
  },
  inputs: {
    marginTop: 10,
    marginBottom: 10,
    marginLeft: 10,
    marginRight: 10
  }
});
