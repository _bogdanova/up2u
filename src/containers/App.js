import React, { Component } from 'react';
import {
  AppRegistry,
  StyleSheet,
  Text,
  View
} from 'react-native';
import {Provider} from 'react-redux';
import {
  Actions, 
  Router, 
  Scene,
  Modal
} from "react-native-router-flux";

import TabIcon from '../components/TabIcon';

//bootstap actions
import {setPlatform, setVersion} from '../actions/device/deviceActions'
import {setStore, setScene} from '../actions/global/globalActions'

//initial state
import configureStore from '../lib/configureStore'
import DeviceInitialState from '../stores/deviceInitialState'
import GlobalInitialState from '../stores/globalInitialState'

//screens
var Launch = require('../containers/Launch');
var Login = require('../containers/Login');
var Profile = require('../containers/Profile');
var Main = require('../containers/Main');

//версия приложения из конфига
import pack from '../../package'
var VERSION = pack.version;

function getInitialState () {
  let _initState = {
    device: (new DeviceInitialState()).set('isMobile', true),
    global: (new GlobalInitialState()),
  }
  return _initState
}

export default function native(platform) {
  class App extends Component {
    constructor(props) {
      super(props);
      this.state = {currentScene:'main'};
    }

    render() {
      //инициализация store со всех редьюсеров
      let store = configureStore(getInitialState());
      store.dispatch(setPlatform(platform));
      store.dispatch(setVersion(VERSION));
      store.dispatch(setScene(this.state.currentScene));
      store.dispatch(setStore(store));

      return (
        <Provider store={store}>
          <Router>
              <Scene key="root" styles={styles.container}>
                <Scene key="launch" component={Launch} title='Launch scene' initial={(this.state.currentScene === 'launch')? true : false}/>
                <Scene key="login" component={Login} title="Login scene" initial={(this.state.currentScene === 'login')? true : false} />

                 <Scene key='tabbar'
                  tabs={true} 
                  tabBarStyle={styles.tabBar}>

                  <Scene key='Main'
                    title='main'
                    icon={TabIcon}
                    iconName={"home"}
                    component={Main}
                    onRight={()=>alert("Right button")} 
                    rightTitle="Right"
                    />

                  <Scene key='Profile'
                    title='profile'
                    icon={TabIcon}
                    iconName={"gear"}
                    component={Profile}   />

                    <Scene key='launch'
                    title='Launch'
                    icon={TabIcon}
                    iconName={"cubes"}
                    initial = {true}
                    component={Launch}   />
                </Scene>
              </Scene>
            
          </Router>
        </Provider>
      );
    }

  }

  //стили нужно вынести отдельно
  const styles = StyleSheet.create({
    container: {
      flex: 1,
      justifyContent: 'center',
      alignItems: 'center',
      backgroundColor: '#F5FCFF',
    },

     tabBar: {
      flex:1,
      flexDirection:'row',
      alignItems:'flex-end',
      backgroundColor: '#000000',
    }
  });

  AppRegistry.registerComponent('up2u', () => App);
}



