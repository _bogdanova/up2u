
'use strict';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';

import globalActions from '../actions/global/globalActions';

import {Map} from 'immutable';

import {Actions} from 'react-native-router-flux';

import React, {Component} from 'react';
import
{
  StyleSheet,
  View
}
from 'react-native';
import Button from 'react-native-button';


/*function mapStateToProps(state) {
    return {
        global: {
            currentState: state.global.currentState,
            showState: state.global.showState
        },
    };
};


function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators({...globalActions}, dispatch),
  };
}*/

var styles = StyleSheet.create({
  container: {
    flexDirection: 'column',
    flex: 1
  },
  button: {
    backgroundColor: '#FF3366',
    borderColor:  '#FF3366',
    marginLeft: 10,
    marginRight: 10
  }
});

/**
 * ## App class
 */
class Main extends Component {
  handlePress() {
    Actions.launch({
      title: 'Launch from main'
    });
  }
  render() {
    return(
      <View style={styles.container}>
    	    <Button style={ styles.button } onPress={ () => this.handlePress() }> На главную</Button>
      </View>
    );
  }
};

module.exports = Main;

/**
 * Connect the properties
 */
//export default connect(mapStateToProps, mapDispatchToProps)(Main);
