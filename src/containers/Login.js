'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text
} from 'react-native';
import Button from 'react-native-button';
import {Actions} from 'react-native-router-flux'

class Login extends React.Component {
    render(){
        return (
            <View style={styles.container}>
                <Text>Login page: {this.props.title}</Text>
                <Text>{this.props.data}</Text>
                <Button onPress={()=> Actions.pop({data:'back from login page', title:'Launch title'})}>Back</Button>
            </View>
        );
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    }
});



module.exports = Login;