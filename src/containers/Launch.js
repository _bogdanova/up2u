'use strict';

import React, { Component } from 'react';
import {
  StyleSheet,
  View,
  Text,
  TouchableHighlight
} from 'react-native';
import Button from 'react-native-button';

import {Actions} from 'react-native-router-flux'
import Icon from 'react-native-vector-icons/FontAwesome' //иконки

class Launch extends React.Component {
    componentDidMount() {
     // Actions.tabbar({message:'tabs', hide:false});    
    }
    render(){
        return (
            <View style={styles.container}>
                <Text>Launch menu {this.props.data}</Text>
                <Button onPress={()=>Actions.login({data:"Custom data", title:'Custom title' })}>Go to Login page</Button>
                 <Button onPress={()=>Actions.Profile({data:"Custom data", title:'Custom title' })}>Go to Profile page</Button>
                <Button onPress={()=>Actions.tabbar({})}> Open Tabbar </Button>
                <Icon name={'cubes'} size={34}/>
            </View>

        );
    }
}

var styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    }
});

module.exports = Launch;
