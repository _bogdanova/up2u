'use strict';

const {
  SET_SESSION_TOKEN,
  SET_STORE,
  SET_STATE,
  GET_STATE,
  SET_SCENE
} = require('../../constants/constants').default;

/**
 * ## set the sessionToken
 */
export function setSessionToken(sessionToken) {
  return {
    type: SET_SESSION_TOKEN,
    payload: sessionToken
  };
}
/**
 * ## set the Redux store
 */
export function setStore(store) {
  return {
    type: SET_STORE,
    payload: store
  };
}
/**
 * ## set state
 */
export function setState(newState) {
  return {
    type: SET_STATE,
    payload: newState
  };
}
/**
 * ## getState
 */
export function getState(toggle) {
  return {
    type: GET_STATE,
    payload: toggle
  };
}

/**
 * ## setScene
 */
export function setScene(scene_id) {
  return {
    type: SET_SCENE,
    payload: scene_id
  };
}
