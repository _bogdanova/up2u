
'use strict';

import {Record} from 'immutable';

var InitialState = Record({
  isMobile: false,
  platform: '',
  version: null
});

export default InitialState;
