
'use strict';

const {Record} = require('immutable');
const {
  REGISTER
} = require('../constants/constants').default;

const Form = Record({
  state: REGISTER,
  disabled: false,
  error: null,
  isValid: false,
  isFetching: false,
  fields: new (Record({
    username: '',
    usernameHasError: false,
    usernameErrorMsg: '',
    email: '',
    emailHasError: false,
    emailErrorMsg: '',
    password: '',
    passwordHasError: false,
    passwordErrorMsg: '',
    passwordAgain: '',
    passwordAgainHasError: false,
    passwordAgainErrorMsg: '',    
    showPassword: false
  }))
});


var InitialState = Record({
  form: new Form
});
export default InitialState;

