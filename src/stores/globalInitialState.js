
'use strict';

import {Record} from 'immutable';
/**
 * ## InitialState
 *  
 * * currentUser - объект, возвращающийся от сервера после успешного логина
 * * showState - отображение текущего стейта в хедере (-)
 * * currentState - JSON-объект текущего стейта
 * * store - Redux store
 *   * device
 *   * global
 *
 */
var InitialState = Record({
  currentUser: null,
  showState: false,
  currentState: null,
  store: null,
  scene: null
});
export default InitialState;
